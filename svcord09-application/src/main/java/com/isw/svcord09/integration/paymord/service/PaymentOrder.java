package com.isw.svcord09.integration.paymord.service;

import org.springframework.http.HttpHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.stereotype.Service;

import com.isw.svcord09.sdk.integration.paymord.entity.PaymentOrderOutput;
import com.isw.svcord09.sdk.integration.paymord.paymord.model.CreatePaymentOrderRequest;
import com.isw.svcord09.sdk.integration.paymord.paymord.model.CreatePaymentOrderResponse;
import com.isw.svcord09.sdk.integration.paymord.paymord.model.CusotmerReference;
import com.isw.svcord09.sdk.integration.paymord.paymord.model.PaymentInitiatorSchema;
import com.isw.svcord09.sdk.integration.paymord.paymord.model.CreatePaymentOrderRequest.PaymentTypeEnum;
import com.isw.svcord09.sdk.integration.paymord.paymord.provider.PaymentOrderApiPaymord;
import com.isw.svcord09.sdk.integration.paymord.service.PaymentOrderBase;
import com.isw.svcord09.sdk.integration.facade.IntegrationEntityBuilder;

@Service
public class PaymentOrder extends PaymentOrderBase {

  private static final Logger log = LoggerFactory.getLogger(PaymentOrder.class);

  // Autowired for Payament Order API 호출하기 위한 Provider
  @Autowired
  PaymentOrderApiPaymord paymentOrderAPI;

  public PaymentOrder(IntegrationEntityBuilder entityBuilder ) { 
    super(entityBuilder );
  }
  
  @NewSpan
  @Override
  public com.isw.svcord09.sdk.integration.paymord.entity.PaymentOrderOutput execute(com.isw.svcord09.sdk.integration.paymord.entity.PaymentOrderInput paymentOrderInput)  {

    // 2. payment Order 요청
    log.info("PaymentOrder.execute()");

    // Http header 구성
    HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.set("accept", "application/json");
    httpHeaders.set("Content-Type", "application/json");
  
    // Request Body 만들기
    CreatePaymentOrderRequest createPaymentOrderRequest = new CreatePaymentOrderRequest();

    // 주의! 반드시 com.isw.svcord09.sdk.integration.paymord.paymord.model.CusotmerReference을 import해야함
    CusotmerReference customerReference = new CusotmerReference();
    customerReference.setAccountNumber(paymentOrderInput.getAccountNumber());
    customerReference.setAmount(paymentOrderInput.getAmount());

    PaymentInitiatorSchema paymentInitiator = new PaymentInitiatorSchema();
    paymentInitiator.setExternalID(paymentOrderInput.getExternalId());
    paymentInitiator.setExternalService(paymentOrderInput.getExternalSerive());
  
    createPaymentOrderRequest.setCustomerReference(customerReference);
    createPaymentOrderRequest.setPaymentInitiator(paymentInitiator);
    createPaymentOrderRequest.setPaymentType(PaymentTypeEnum.CASH_WITHDRAWAL);
  
    // Payment Order 요청 후 response 내용으로 return하기
    // response 중 우리가 필요한 body만 가져옴 
    CreatePaymentOrderResponse paymentOrderResponse = paymentOrderAPI.createPaymentOrder(createPaymentOrderRequest, httpHeaders).getBody();
  
    PaymentOrderOutput paymentOrderOutput = this.entityBuilder.getPaymord().getPaymentOrderOutput().build();
    paymentOrderOutput.setPaymentOrderReulst(paymentOrderResponse.getContents().getPaymentOrderResult());
    paymentOrderOutput.setTransactionId(paymentOrderOutput.getTransactionId());
    
    return paymentOrderOutput;
  }

}
