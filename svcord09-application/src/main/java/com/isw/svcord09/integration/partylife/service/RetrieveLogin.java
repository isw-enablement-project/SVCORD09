package com.isw.svcord09.integration.partylife.service;

import org.springframework.http.HttpHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.stereotype.Service;

import com.isw.svcord09.sdk.integration.partylife.entity.RetrieveLoginOutput;
import com.isw.svcord09.sdk.integration.partylife.partylife.model.LoginResponse;
import com.isw.svcord09.sdk.integration.partylife.partylife.provider.LoginApiPartylife;
import com.isw.svcord09.sdk.integration.partylife.service.RetrieveLoginBase;
import com.isw.svcord09.sdk.integration.facade.IntegrationEntityBuilder;

@Service
public class RetrieveLogin extends RetrieveLoginBase {

  private static final Logger log = LoggerFactory.getLogger(RetrieveLogin.class);

  @Autowired
  LoginApiPartylife partyLife;

  public RetrieveLogin(IntegrationEntityBuilder entityBuilder ) { 
    super(entityBuilder );
  }
  
  @NewSpan
  @Override
  public com.isw.svcord09.sdk.integration.partylife.entity.RetrieveLoginOutput execute(com.isw.svcord09.sdk.integration.partylife.entity.RetrieveLoginInput retrieveLoginInput)  {

    // Integration
    // 1. Party Life Cycle Management에서 User확인
    log.info("RetrieveLogin.execute()");
    
    // 헤더 만들고
    HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.set("accept", "application/json");
    httpHeaders.set("Content-Type", "application/json");  
  

    // request 보냄. 이 과정은 LoginApiPartylife에 구현되어 있음 
    // response 중 우리가 필요한 body만 가져옴
    LoginResponse loginResponse = partyLife.getUserLogin(retrieveLoginInput.getId(), httpHeaders).getBody();
  
    // 로그인 결과 
    RetrieveLoginOutput loginOuput = this.entityBuilder.getPartylife().getRetrieveLoginOutput().build();
    loginOuput.setResult(loginResponse.getResult().getValue());
  
    return loginOuput;
  }

}
