package com.isw.svcord09.api.v1;

import com.isw.svcord09.domain.svcord.service.WithdrawalDomainService;
import com.isw.svcord09.sdk.api.v1.api.WithddrawalApiV1Delegate;
import org.springframework.stereotype.Service;
import org.apache.http.client.entity.EntityBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import com.isw.svcord09.sdk.api.v1.model.ErrorSchema;
import com.isw.svcord09.sdk.api.v1.model.WithdrawalBodySchema;
import com.isw.svcord09.sdk.api.v1.model.WithdrawalResponseSchema;
import com.isw.svcord09.sdk.domain.facade.DomainEntityBuilder;
import com.isw.svcord09.sdk.domain.svcord.entity.WithdrawalDomainServiceInput;
import com.isw.svcord09.sdk.domain.svcord.entity.WithdrawalDomainServiceOutput;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * A stub that provides implementation for the WithddrawalApiV1Delegate
 */
@SuppressWarnings("unused")
@Service
@ComponentScan(basePackages = "com.isw.svcord09.sdk.api.v1.api")
public class WithddrawalApiV1Provider implements WithddrawalApiV1Delegate {

  /*
   * 각 클래스에서 사용하는 서비스는(Annotation을 호출하기 위해서는) @autowired를 이용하여 부른다.
   * Annotation으로 이루어진 class는 인스턴스를 하나만 생성하여 쓰기 때문에
   * 생성된 것을 가져다 쓰겠다는 의미 임
   */
  @Autowired
  WithdrawalDomainService withdrawalDomainService;

  @Autowired
  DomainEntityBuilder entityBuilder;

  @Override
  public ResponseEntity<WithdrawalResponseSchema> servicingOrderWithdrawalPost(
      WithdrawalBodySchema withdrawalBodySchema) {

    //
    WithdrawalDomainServiceInput withdrawalInput = entityBuilder.getSvcord()
        .getWithdrawalDomainServiceInput()
        .setAccountNumber(withdrawalBodySchema.getAccountNumber())
        .setAmount(withdrawalBodySchema.getAmount())
        .build();

    // 서비스 호출
    withdrawalDomainService.execute(withdrawalInput);

    WithdrawalDomainServiceOutput withdrawalDomainServiceOutput;
    withdrawalDomainServiceOutput = withdrawalDomainService.execute(withdrawalInput);

    if (withdrawalDomainServiceOutput == null) {

    }

    return ResponseEntity.ok(null);
  }

}
