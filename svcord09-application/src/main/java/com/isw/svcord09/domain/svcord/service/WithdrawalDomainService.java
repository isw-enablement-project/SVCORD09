package com.isw.svcord09.domain.svcord.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.stereotype.Service;

import com.isw.svcord09.sdk.domain.svcord.entity.CreateServicingOrderProducerInput;
import com.isw.svcord09.sdk.domain.svcord.entity.CustomerReferenceEntity;
import com.isw.svcord09.sdk.domain.svcord.entity.Svcord01;
import com.isw.svcord09.sdk.domain.svcord.entity.UpdateServicingOrderProducerInput;
import com.isw.svcord09.sdk.domain.svcord.entity.WithdrawalDomainServiceOutput;
import com.isw.svcord09.sdk.domain.svcord.service.WithdrawalDomainServiceBase;
import com.isw.svcord09.sdk.domain.svcord.type.ServicingOrderWorkResult;
import com.isw.svcord09.sdk.integration.facade.IntegrationEntityBuilder;
import com.isw.svcord09.sdk.integration.partylife.entity.RetrieveLoginInput;
import com.isw.svcord09.sdk.integration.partylife.entity.RetrieveLoginOutput;
import com.isw.svcord09.sdk.integration.paymord.entity.PaymentOrderInput;
import com.isw.svcord09.sdk.integration.paymord.entity.PaymentOrderOutput;
import com.isw.svcord09.domain.svcord.command.Svcord01Command;
import com.isw.svcord09.integration.partylife.service.RetrieveLogin;
import com.isw.svcord09.integration.paymord.service.PaymentOrder;
import com.isw.svcord09.sdk.domain.facade.DomainEntityBuilder;
import com.isw.svcord09.sdk.domain.facade.Repository;

@Service
public class WithdrawalDomainService extends WithdrawalDomainServiceBase {

  private static final Logger log = LoggerFactory.getLogger(WithdrawalDomainService.class);

  // 사용하기 위한 Command, Intergartion services등을 Autowired로 엮는다.
  @Autowired
  Svcord01Command servicingOrderProcedureCommand;

  @Autowired
  PaymentOrder paymentOrder;

  @Autowired
  RetrieveLogin retrieveLogin;

  @Autowired
  IntegrationEntityBuilder integrationEntityBuilder;

  public WithdrawalDomainService(DomainEntityBuilder entityBuilder, Repository repo) {

    super(entityBuilder, repo);

  }

  @NewSpan
  @Override
  public com.isw.svcord09.sdk.domain.svcord.entity.WithdrawalDomainServiceOutput execute(
      com.isw.svcord09.sdk.domain.svcord.entity.WithdrawalDomainServiceInput withdrawalDomainServiceInput) {

    log.info("WithdrawalDomainService.execute()");

    // 1. 사용자 인증요청 Party Life Cycle Management 호출
    RetrieveLoginInput retrieveInput = integrationEntityBuilder.getPartylife()
        .getRetrieveLoginInput()
        // test1이라고 하드코딩. 왜냐하면 id를 받아오지 않기 때문에
        .setId("test1")
        .build();

    RetrieveLoginOutput retrieveLoginOutput = retrieveLogin.execute(retrieveInput);

    // 2. User Valid 체크
    if (retrieveLoginOutput.getResult() != "SUCCESS") {
      return null;
    }

    // 3. Root Entity 호출하여 새로운 레코드 생성
    CustomerReferenceEntity customerReference = this.entityBuilder.getSvcord()
        .getCustomerReference()
        .build();
    customerReference.setAccountNumber(withdrawalDomainServiceInput.getAccountNumber());
    customerReference.setAmount(withdrawalDomainServiceInput.getAmount());

    CreateServicingOrderProducerInput createIntput = this.entityBuilder.getSvcord()
        .getCreateServicingOrderProducerInput()
        .build();
    createIntput.setCustomerReference(customerReference);
    Svcord01 createOut = servicingOrderProcedureCommand.createServicingOrderProducer(createIntput);

    // 4. 현금 출금 요청을 위한 Payment Order 호출
    PaymentOrderInput paymentOrderInput = integrationEntityBuilder.getPaymord()
        .getPaymentOrderInput()
        .build();
    paymentOrderInput.setAccountNumber(withdrawalDomainServiceInput.getAccountNumber());
    paymentOrderInput.setAmount(withdrawalDomainServiceInput.getAmount());
    paymentOrderInput.setExternalId("SVCORD09");
    paymentOrderInput.setExternalSerive("SVCORD09");
    paymentOrderInput.setPaymentType("CASH_WITHDRAWL");

    PaymentOrderOutput paymentOrderOutput = paymentOrder.execute(paymentOrderInput);

    // 5. Payment order결과를 DB에 업데이트 하고 해당 아웃풋을 전달
    UpdateServicingOrderProducerInput updateServicingOrderProducerInput = this.entityBuilder.getSvcord()
        .getUpdateServicingOrderProducerInput()
        .build();
    updateServicingOrderProducerInput.setUpdateID(createOut.getId().toString());
    updateServicingOrderProducerInput
        .setServicingOrderWorkResult(ServicingOrderWorkResult.valueOf(paymentOrderOutput.getPaymentOrderReulst()));

    servicingOrderProcedureCommand.updateServicingOrderProducer(createOut, updateServicingOrderProducerInput);

    WithdrawalDomainServiceOutput withdrawalDomainServiceOutput = this.entityBuilder.getSvcord()
        .getWithdrawalDomainServiceOutput()
        .setServicingOrderWorkResult(ServicingOrderWorkResult.valueOf(paymentOrderOutput.getPaymentOrderReulst()))
        .setTrasactionId(paymentOrderOutput.getTransactionId())
        .build();

    return withdrawalDomainServiceOutput;
  }

}
