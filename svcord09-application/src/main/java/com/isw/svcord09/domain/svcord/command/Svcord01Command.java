package com.isw.svcord09.domain.svcord.command;


import org.springframework.stereotype.Service;
import com.isw.svcord09.sdk.domain.svcord.command.Svcord01CommandBase;
import com.isw.svcord09.sdk.domain.svcord.entity.Svcord01Entity;
import com.isw.svcord09.sdk.domain.svcord.entity.ThirdPartyReferenceEntity;
import com.isw.svcord09.sdk.domain.svcord.type.ServicingOrderType;
import com.isw.svcord09.sdk.domain.svcord.type.ServicingOrderWorkProduct;
import com.isw.svcord09.sdk.domain.svcord.type.ServicingOrderWorkResult;
import com.isw.svcord09.sdk.domain.facade.DomainEntityBuilder;
import com.isw.svcord09.sdk.domain.facade.Repository;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class Svcord01Command extends Svcord01CommandBase {

  private static final Logger log = LoggerFactory.getLogger(Svcord01Command.class);

  public Svcord01Command(DomainEntityBuilder entityBuilder,  Repository repo ) {
    super(entityBuilder,  repo );
  }

  
    
    @Override
    public com.isw.svcord09.sdk.domain.svcord.entity.Svcord01 createServicingOrderProducer(com.isw.svcord09.sdk.domain.svcord.entity.CreateServicingOrderProducerInput createServicingOrderProducerInput)  {
    
      // Command 
      // 1. Create Control Record
      log.info("Svcord01Command.createServicingOrderProducer()");
      
      // entity 껍데기 만들고
      Svcord01Entity servicingOrderProcedure = this.entityBuilder.getSvcord().getSvcord01().build();
      
      // 파라미터로 받아온 값을 set해서 
      servicingOrderProcedure.setCustomerReference(createServicingOrderProducerInput.getCustomerReference());
      servicingOrderProcedure.setProcessStartDate(LocalDate.now());
      servicingOrderProcedure.setServicingOrderType(ServicingOrderType.PAYMENT_CASH_WITHDRAWALS);
      servicingOrderProcedure.setServicingOrderWorkDescription("CASH WITHDRAWALS");
      servicingOrderProcedure.setServicingOrderWorkProduct(ServicingOrderWorkProduct.PAYMENT);
      servicingOrderProcedure.setServicingOrderWorkResult(ServicingOrderWorkResult.PROCESSING);
      
      ThirdPartyReferenceEntity thirdPartyReference = new ThirdPartyReferenceEntity();
      thirdPartyReference.setId("test1");
      thirdPartyReference.setPassword("password");
      
      servicingOrderProcedure.setThirdPartyReference(thirdPartyReference);
      
      // 마지막에 JPA save함수로 DB적재 실행
      return repo.getSvcord().getSvcord01().save(servicingOrderProcedure);
    }
  
    
    @Override
    public void updateServicingOrderProducer(com.isw.svcord09.sdk.domain.svcord.entity.Svcord01 instance, com.isw.svcord09.sdk.domain.svcord.entity.UpdateServicingOrderProducerInput updateServicingOrderProducerInput)  { 

      // Command 
      // 2. Update Control Record
      log.info("Svcord01Command.updateServicingOrderProducer()");

      // entity 껍데기 만들고
      // 이 때 referenceid를 기준으로 select해서 가져옴
      Svcord01Entity servicingOrderProcedure = this.repo.getSvcord().getSvcord01().getReferenceById(Long.parseLong(updateServicingOrderProducerInput.getUpdateID()));
      
      // 파라미터로 받아온 값을 set해서 
      servicingOrderProcedure.setCustomerReference(updateServicingOrderProducerInput.getCustomerReference());
      servicingOrderProcedure.setProcessEndDate(LocalDate.now());
      servicingOrderProcedure.setServicingOrderWorkResult(updateServicingOrderProducerInput.getServicingOrderWorkResult());
      servicingOrderProcedure.setThirdPartyReference(updateServicingOrderProducerInput.getThirdPartyReference());
    
      // 로그 
      log.info(updateServicingOrderProducerInput.getUpdateID().toString());
      log.info(updateServicingOrderProducerInput.getCustomerReference().getAccountNumber());
      log.info(updateServicingOrderProducerInput.getCustomerReference().getAmount());
    
      // 마지막에 JPA save함수로 DB업데이트 실행
      this.repo.getSvcord().getSvcord01().save(servicingOrderProcedure);
    }
  
}
